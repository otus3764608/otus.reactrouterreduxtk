import { createSlice } from "@reduxjs/toolkit";
import { User } from "../models/user";

export const userSlice = createSlice({
  name: "user",
  initialState: {
    user: undefined,
    counter: 0,
  },
  reducers: {
    userSet: (state, value) => {
      state.user = value.payload;
    },
    increase: (state) => {
      state.counter += 1;
    },
    decrease: (state) => {
      state.counter -= 1;
    },
  },
});

// each case under reducers becomes an action
export const { increase, decrease } = userSlice.actions;

export default userSlice.reducer;
