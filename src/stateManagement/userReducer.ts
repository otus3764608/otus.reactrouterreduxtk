import { OrderThing } from "../models/orderThing";
import { User } from "../models/user";

export const OrderState = Object.freeze({
    USER_SET: '[USER_STATE] USER_SET',
    INCR: '[USER_STATE] INCR',
    DECR: '[USER_STATE] DECR',
});


export const Actions = Object.freeze({
    setUser: (user: User) => ({ type: OrderState.USER_SET, payload: user })
});

interface State {
    user: User | null;
    counter: number;
}



const initialState: State = {
    user: null,
    counter: 15
}


const userReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case OrderState.USER_SET:
    
            return { ...state, user: action.payload };

        case OrderState.INCR:
            return { ...state, counter: state.counter + 1 };
        case OrderState.DECR:
            return { ...state, counter: state.counter - 1 };
        default:
            return state;
    }
};
export default userReducer;