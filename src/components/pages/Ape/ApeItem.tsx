
import { useDispatch } from "react-redux";
import { User } from "../../../models/user";
import { Actions } from "../../../stateManagement/userReducer";

interface Props {
    user: User;
}

const ApeItem = (props: Props) => {
    const { user } = props;
    const dispatch = useDispatch();

    const css = {
        borderRadius: "16px",
        border: "1px solid green",
        margin: "10px",
    };

    const divimgCss = {
        "borderRadius": "10px",
    };

    const imgcss = {
        height: '100px',
        objectFit: 'cover',
        width: '100px'
    } as any;

    const select = () => {
        dispatch(Actions.setUser(user));
    };

    const span = {
        display: 'inline'
    };
    return <div style={css}>
        <div style={divimgCss}>
            <img style={imgcss} src={user.imgUrl} />
        </div>
        <div style={span}>{user.username}</div>
        <button onClick={select}>Выбрать</button>
    </div>;
}

export default ApeItem;