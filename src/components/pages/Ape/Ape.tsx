
import { connect, useSelector } from "react-redux";
import { useParams } from "react-router";
import { User } from "../../../models/user";

interface Props {
    selectedApe?: User;
    list?:any;
}

const Ape = (props: Props) => {
    const { selectedApe , list} = props;


const qp=useParams();

    const counter = useSelector((x: any) => x.user);

    const css = {
        borderRadius: "16px",
        border: "1px solid green",
        margin: "10px",
    };

    const divimgCss = {

        "borderRadius": "10px",
    };

    const imgcss = {
        height: '200px',
        objectFit: 'cover',
        width: '200px'
    } as any;


    const span = {
        display: 'inline'
    };
    return <div style={css}>
        <label>вы выбрали</label>
        <div style={divimgCss}>
            <img style={imgcss} src={selectedApe?.imgUrl} />
        </div>
        <div style={span}>{selectedApe?.username}</div>
{JSON.stringify(list)}
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<h1>{JSON.stringify(counter)}</h1>
    </div>;
}
// Функция возвращает объект с нужными нам данными
// Из редакс
// При этом поля в объекте
// Должны называться также
// как и пропсы,
// с которым мы его соединяем
const mapStateToProps = (state: any) => {
    console.log(state.user);
    return { selectedApe: state.user.user, list:state.order.list };
}


// При помощи connect
// Мы соединяем данные redux из mapStateToProps
// С пропсами компонента Thunk
export default connect(mapStateToProps)(Ape);