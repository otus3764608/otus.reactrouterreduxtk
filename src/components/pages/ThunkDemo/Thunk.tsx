import { connect, useDispatch, useSelector } from "react-redux";
import { Actions } from "../../../stateManagement/thunkReducer";

interface Props {
    data?: any;
}

export function fetchTodoById() {
    // fetchTodoByIdThunk is the "thunk function"
    return async function fetchTodoByIdThunk(dispatch: any, getState: any) {
        const response = await fetch(`https://jsonplaceholder.typicode.com/users`);
        const json = await response.json();
        dispatch(Actions.setData(JSON.stringify(json)))
    }
}


function Thunk(props: Props) {

    const dispatch = useDispatch();


    const thunkIt = () => {
        //    dispatch(fetchTodoById());
    };

    return <>
        <button onClick={thunkIt}>Запускай thunk</button>
        <br />
        <span style={{ fontSize: '30px' }}>{JSON.stringify(props.data)}</span>
    </>;
}


// Функция возвращает объект с нужными нам данными
// Из редакс
// При этом поля в объекте
// Должны называться также
// как и пропсы,
// с которым мы его соединяем
const mapStateToProps = (state: any) => {
    return { data: state.funky.fancyData };
}


// При помощи connect
// Мы соединяем данные redux из mapStateToProps
// С пропсами компонента Thunk
export default connect(mapStateToProps)(Thunk);